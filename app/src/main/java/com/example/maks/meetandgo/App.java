package com.example.maks.meetandgo;

import android.app.Application;

import timber.log.Timber;

/**
 * Created by maks on 31.10.17.
 */

public class App extends Application {
    @Override public void onCreate() {
        super.onCreate();

        Timber.plant(new Timber.DebugTree());
    }
}
