package com.example.maks.meetandgo;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.maks.meetandgo.Models.User;
import com.google.gson.Gson;

import butterknife.BindView;
import butterknife.ButterKnife;
import timber.log.Timber;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.editTextLogin)
    EditText emailEditText;

    @BindView(R.id.editTextPassword)
    EditText passwordEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Авторизация");
    }

    public void onLogin(View view) {
        String email = emailEditText.getText().toString();
        String password = passwordEditText.getText().toString();

        if (!isValidTextNotify("email", email)) return;
        if (!isValidTextNotify("password", password)) return;

        User user = new User(email, password, "");

        Intent activity = new Intent(this, UserDetailActivity.class);
        activity.putExtra("user", new Gson().toJson(user));
        startActivity(activity);
    }

    private boolean isValidTextNotify(String name, String value) {

        if (!isValidText(value) ) {
            String text = name + " is invalid!";

            Context context = getApplicationContext();
            int duration = Toast.LENGTH_SHORT;
            Toast toast = Toast.makeText(context, text, duration);
            toast.show();

            return false;
        }

        return true;
    }

    public void OnRegistry(View view) {
        Intent intent = new Intent(this, RegistryActivity.class);
        startActivity(intent);
    }

    private boolean isValidText(String text) {
        if (text == null || text.isEmpty()) {
            return false;
        }

        return true;
    }
}
